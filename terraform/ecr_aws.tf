terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.68.0"
    }
  }
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/32043871/terraform/state/prod"
    lock_address   = "https://gitlab.com/api/v4/projects/32043871/terraform/state/prod/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/32043871/terraform/state/prod/lock"
    username       = "gitlab-ci-token"
    lock_method    = "POST"
    unlock_method  = "DELETE"
  }
}

provider "aws" {
  region = "{AWS_REGION}"
  shared_credentials_file = "./credentials"
}

resource "aws_ecr_repository" "{APP_PROD}" {
  name                 = "{APP_PROD}"

  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "aws_ecr_repository" "{NGINX_PROD}" {
  name                 = "{NGINX_PROD}"

  image_scanning_configuration {
    scan_on_push = true
  }
}
#resource "aws_ecr_repository_policy" "{APP_PROD}_policy" {
#  repository = aws_ecr_repository.{APP_PROD}.name

#  policy = <<EOF

#{
#  "Version": "2021-12-13",
#  "Statement": [{
#    "Sid": "ECR Repository Policy",
#    "Effect": "Allow",
#    "Principal": {
#      "AWS": "arn:aws:iam:{AWS_REGION}:{AWS_ACCESS_ID}:user/developer"
#    },
#    "Action": [
#      "ecr:PutImageTagMutability",
#      "ecr:StartImageScan",
#      "ecr:DescribeImageReplicationStatus",
#      "ecr:ListTagsForResource",
#      "ecr:UploadLayerPart",
#      "ecr:BatchDeleteImage",
#      "ecr:ListImages",
#      "ecr:BatchGetRepositoryScanningConfiguration",
#      "ecr:DeleteRepository",
#      "ecr:CompleteLayerUpload",
#      "ecr:TagResource",
#      "ecr:DescribeRepositories",
#      "ecr:BatchCheckLayerAvailability",
#      "ecr:ReplicateImage",
#      "ecr:GetLifecyclePolicy",
#      "ecr:PutLifecyclePolicy",
#      "ecr:DescribeImageScanFindings",
#      "ecr:GetLifecyclePolicyPreview",
#      "ecr:CreateRepository",
#      "ecr:PutImageScanningConfiguration",
#      "ecr:GetDownloadUrlForLayer",
#      "ecr:DeleteLifecyclePolicy",
#      "ecr:PutImage",
#      "ecr:UntagResource",
#      "ecr:BatchGetImage",
#      "ecr:DescribeImages",
#      "ecr:StartLifecyclePolicyPreview",
#      "ecr:InitiateLayerUpload",
#      "ecr:GetRepositoryPolicy"
#    ],
#    "Resource": [
#      "arn:aws:ecr:{AWS_REGION}:{AWS_ACCESS_ID}:repository/{APP_PROD}"
#    ]
#    }]
#}
#EOF
#}

#resource "aws_ecr_repository_policy" "{NGINX_PROD}_policy" {
#  repository = aws_ecr_repository.{NGINX_PROD}.name

#  policy = <<EOF

#{
#  "Version": "2021-12-13",
#  "Statement": [{
#    "Sid": "ECR Repository Policy",
#    "Effect": "Allow",
#    "Principal": {
#      "AWS": "arn:aws:iam:{AWS_REGION}:{AWS_ACCESS_ID}:user/developer"
#    },
#    "Action": [
#      "ecr:PutImageTagMutability",
#      "ecr:StartImageScan",
#      "ecr:DescribeImageReplicationStatus",
#      "ecr:ListTagsForResource",
#      "ecr:UploadLayerPart",
#      "ecr:BatchDeleteImage",
#      "ecr:ListImages",
#      "ecr:BatchGetRepositoryScanningConfiguration",
#      "ecr:DeleteRepository",
#      "ecr:CompleteLayerUpload",
#      "ecr:TagResource",
#      "ecr:DescribeRepositories",
#      "ecr:BatchCheckLayerAvailability",
#      "ecr:ReplicateImage",
#      "ecr:GetLifecyclePolicy",
#      "ecr:PutLifecyclePolicy",
#      "ecr:DescribeImageScanFindings",
#      "ecr:GetLifecyclePolicyPreview",
#      "ecr:CreateRepository",
#      "ecr:PutImageScanningConfiguration",
#      "ecr:GetDownloadUrlForLayer",
#      "ecr:DeleteLifecyclePolicy",
#      "ecr:PutImage",
#      "ecr:UntagResource",
#      "ecr:BatchGetImage",
#      "ecr:DescribeImages",
#      "ecr:StartLifecyclePolicyPreview",
#      "ecr:InitiateLayerUpload",
#      "ecr:GetRepositoryPolicy"
#    ],
#    "Resource": [
#      "arn:aws:ecr:{AWS_REGION}:{AWS_ACCESS_ID}:repository/{NGINX_PROD}"
#    ]
#    }]
#}
#EOF
#}
