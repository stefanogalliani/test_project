# Project documentation


## 0 - Variables
### 0.1 - settings/ci_cd variables
Inside Settings -> CI/CD some variables have been set for security reasons.

![](docs/cicd_variables.png)

### 0.2 - .gitlab-ci.yml variables

#### 0.2.1 - Terraform variables
Controlling Terraform output in automation as described [here](https://learn.hashicorp.com/tutorials/terraform/automate-terraform?in=terraform/automation&utm_source=WEBSITE&utm_medium=WEB_IO&utm_offer=ARTICLE_PAGE&utm_content=DOCS#controlling-terraform-output-in-automation):

```
TF_IN_AUTOMATION: "true"

```
`TF_HTTP_PASSWORD` normally is used the Personal Access Token for authentication. By running it via GitLab CI, it is the contents of the ${CI_JOB_TOKEN} CI/CD variable.

This password is used to manage Terraform state backend as a remote data source. This preserves the state and allows you to run the pipeline as many times as you like. Without preserved state, the "terraform apply" command would fail when AWS ECR is already present.
Documentation is [here](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html#using-a-gitlab-managed-terraform-state-backend-as-a-remote-data-source).

```
TF_HTTP_PASSWORD: ${CI_JOB_TOKEN}
```
#### 0.2.2 - Docker variables

When using dind service, you must set `DOCKER_HOST` variable in order to talk with the daemon started inside of the service `docker:dind`:
```
DOCKER_HOST: tcp://docker:2375
```
#### 0.2.3 - AWS variables
Here are reported default region and ECR URI:
```
AWS_REGION: "us-east-1"
AWS_REGISTRY: "$AWS_ACCESS_ID.dkr.ecr.${AWS_REGION}.amazonaws.com"
```
#### 0.2.4 - Application stack variables
Informations about Nginx and flask application:
```
  APP_NAME: "app"
  APP_TAG: "1.0"
  ### Dev
  APP_DEV: "${APP_NAME}_dev"
  APP_DEV_TAG: "${APP_TAG}:${APP_TAG}"
  ### Prod
  APP_PROD: "${APP_NAME}_prod"
  APP_PROD_TAG: "${APP_PROD}:${APP_TAG}"

  ## Nginx
  NGINX_NAME: "nginx"
  NGINX_TAG: "1.0"
  ### Dev
  NGINX_DEV: "${NGINX_NAME}_dev"
  NGINX_DEV_TAG: "${NGINX_DEV}:${NGINX_TAG}"
  ### Prod
  NGINX_PROD: "${NGINX_NAME}_prod"
  NGINX_PROD_TAG: "${NGINX_PROD}:${NGINX_TAG}"

```
If you need to change the app name or tag, you can modify it from here without having to apply other changes in other files.

### 0.3 - docker-compose variables
docker-compose files are located [here](https://gitlab.com/stefanogalliani/test_project/-/tree/main).
docker-compose are completely variabilizated:

- variables from .env file: `$VARIABLE`
- variables from .gitlab-ci.yml: `${VARIABLE}`
- variables which will be modified later by the pipeline: `{VARIABLE}`. This is needed because docker-compose services's name cannot be a variable.

The trick to this last is explained inside .gitlab-ci.yml:
```
sed -i 's/{VARIABLE INSIDE COMPOSE}/'${VARIABLE_DECLARED IN .gitlab-ci.yml}'/g' docker-compose-[env].yml
```

Example:

.gitlab-ci.yml:
```
variables:
  APP_NAME: "app"
  NGINX_NAME: "nginx"  
```
```
before_script:
  - sed -i 's/{SERVICE_APP_DEV}/'${APP_DEV}'/g' docker-compose-dev.yml
  - sed -i 's/{SERVICE_NGINX_DEV}/'${NGINX_DEV}'/g' docker-compose-dev.yml  
```

docker-compose-dev.yml:
```
version: "3.9"
services:
  {SERVICE_APP_DEV}:
[...]
  {SERVICE_NGINX_DEV}:
    environment:
[...]
```

### 0.3.1 - .env variables
.env configuration file is used by docker-compose to read variables.
As the docker-compose files, there's a .env file for each of the environment.

### 0.3.2 - nginx variables
nginx variables are read out from .env file. Inside [container_app/nginx/conf/ directory](https://gitlab.com/stefanogalliani/test_project/-/tree/main/container_app/nginx/conf), there’s a nginx default.conf template where variables will be substituted with [containter_app/nginx/nginx-variables_setup.sh script](https://gitlab.com/stefanogalliani/test_project/-/blob/main/container_app/nginx/nginx-variables_setup.sh).
The mentioned script is the nginx Dockerfile’s entrypoint.

Template from [here](container_app/nginx/conf/default.conf.template):
```
server {

    listen 80;
    server_name $NGINX_SERVER_ADDRESS;

    location / {
        proxy_pass http://$NGINX_LOCATION:$NGINX_LOCATION_PORT;

        # Do not change this
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location /static {
        rewrite ^/static(.*) /$1 break;
        root /static;
    }
}
```
Script from [here](container_app/nginx/nginx-variables_setup.sh):
```
#!/usr/bin/env sh
set -eu

envsubst '${NGINX_SERVER_ADDRESS} ${NGINX_LOCATION} ${NGINX_LOCATION_PORT}' < /etc/nginx/conf.d/default.conf.template > /etc/nginx/conf.d/default.conf

exec "$@"
```
After a build and run:

![](docs/nginx_after_template.png)

## 1 - Development environment
### 1.1 - Dockerfile
Inside [container/app folder](https://gitlab.com/stefanogalliani/test_project/-/tree/main/container_app) are located flask app's Dockerfiles.
Both, one for development and one for production, contain the `git clone` command to minimal-flask-example repository in order to remove all problems related to submodules (repository inside another repository).
```
RUN git clone https://github.com/matdoering/minimal-flask-example.git /app/minimal-flask-example

```
Dockerfile build is integrated into docker-compose with `build` module.

### 1.2 - docker-compose
Since .env files are present, to bring the docker-compose up is necessary to run this:
```
docker-compose -f docker-compose-[env].yml --env-file .env.[env] up --build
```
As it's development, it becomes:
```
docker-compose -f docker-compose-dev.yml --env-file .env.dev up --build
```
### 1.3 - Testing
It's possible to test it by running a curl:
![](docs/docker-compose-dev-test.png)

Execute `pytest` inside the directory of repository in order to perform a full test:
![](docs/docker-compose-dev-pytest.png)

As it can be seen, nginx is proxing and logging every request.

Full documentation about test [here](https://github.com/matdoering/minimal-flask-example/blob/master/README.md).

## 2 - Build the infrastructure
### 2.1 - Terraform configurations
Inside [terraform directory](https://gitlab.com/stefanogalliani/test_project/-/tree/main/terraform), there are credentials and ecr_aws.tf files.

`credential` file is used to login to AWS.

Instead, in this section, there are required providers and backend to preserve status as described in section "0.2.1 - Terraform variables":
```
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.68.0"
    }
  }
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/32043871/terraform/state/prod"
    lock_address   = "https://gitlab.com/api/v4/projects/32043871/terraform/state/prod/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/32043871/terraform/state/prod/lock"
    username       = "gitlab-ci-token"
    lock_method    = "POST"
    unlock_method  = "DELETE"
  }
}
```
It may happen that Terraform returns "Error: Error acquiring the state lock" while parallel access to the state lock.
To workaround this problem, it's necessary to add force-unlock after init:
```
terraform force-unlock -force [ID]
```

For example:
```
Acquiring state lock. This may take a few moments...
╷
│ Error: Error acquiring the state lock
│ 
│ Error message: HTTP remote state already locked:
│ ID=16991042-26e9-a65f-5dfc-54f85c9e9c8d
````

Unlock:
```
    - terraform init -reconfigure -input=false
    - terraform force-unlock -force 16991042-26e9-a65f-5dfc-54f85c9e9c8d
    - terraform plan 
    - terraform apply -auto-approve -input=false

```

Then there are ECR definition, one per image for ease of management and of course security:
```
resource "aws_ecr_repository" "{APP_PROD}" {
  name                 = "{APP_PROD}"

  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "aws_ecr_repository" "{NGINX_PROD}" {
  name                 = "{NGINX_PROD}"

  image_scanning_configuration {
    scan_on_push = true
  }
}
```
Lastly, in the same file the policies for each ECR are defined:

```
resource "aws_ecr_repository_policy" "{APP_PROD}_policy" {
  repository = aws_ecr_repository.{APP_PROD}.name

  policy = <<EOF

{
  "Version": "2021-12-13",
  "Statement": [{
    "Sid": "ECR Repository Policy",
    "Effect": "Allow",
    "Principal": {
      "AWS": "arn:aws:iam:{AWS_REGION}:{AWS_ACCESS_ID}:user/developer"
    },
    "Action": [
      "ecr:PutImageTagMutability",
      "ecr:StartImageScan",
      "ecr:DescribeImageReplicationStatus",
      "ecr:ListTagsForResource",
      "ecr:UploadLayerPart",
      "ecr:BatchDeleteImage",
      "ecr:ListImages",
      "ecr:BatchGetRepositoryScanningConfiguration",
      "ecr:DeleteRepository",
      "ecr:CompleteLayerUpload",
      "ecr:TagResource",
      "ecr:DescribeRepositories",
      "ecr:BatchCheckLayerAvailability",
      "ecr:ReplicateImage",
      "ecr:GetLifecyclePolicy",
      "ecr:PutLifecyclePolicy",
      "ecr:DescribeImageScanFindings",
      "ecr:GetLifecyclePolicyPreview",
      "ecr:CreateRepository",
      "ecr:PutImageScanningConfiguration",
      "ecr:GetDownloadUrlForLayer",
      "ecr:DeleteLifecyclePolicy",
      "ecr:PutImage",
      "ecr:UntagResource",
      "ecr:BatchGetImage",
      "ecr:DescribeImages",
      "ecr:StartLifecyclePolicyPreview",
      "ecr:InitiateLayerUpload",
      "ecr:GetRepositoryPolicy"
    ],
    "Resource": [
      "arn:aws:ecr:{AWS_REGION}:{AWS_ACCESS_ID}:repository/{APP_PROD}"
    ]
    }]
}
EOF
}
```

This section is commented due to this error (copied and pasted from AWS Console, so it needed more time to understand why it happens):
![](docs/policy.png)

### 2.2 - Create AWS ECR
From [terraform directory](https://gitlab.com/stefanogalliani/test_project/-/tree/main/terraform), there are three easy steps to create the ECR from Terraform:
1) terraform init -reconfigure -input=false
2) terraform plan
3) terraform apply -auto-approve -input=false

Repositories creation:
![](docs/repo.png)

### 2.3 - Pushing into AWS ECR
After ECR creation, just login into AWS repo, tag the local image and push into the repo.
1) Login:
```
aws ecr get-login-password | docker login --username AWS --password-stdin ${AWS_REGISTRY}
```
2) Tag: 
```
docker tag [image_name]:[image_tag] ${AWS_ACCESS_ID}.dkr.ecr.${REGION}.amazonaws.com/[image_name]:[image_tag]
```

3) Push: 
```
docker push ${AWS_ACCESS_ID}.dkr.ecr.us-east-1.amazonaws.com/[image_name]:[image_tag]

```
![](docs/ecr_push.png)
![](docs/app_prod_image_push.png)
![](docs/nginx_image_push.png)

## 3 – CI/CD
To run the pipeline, from GitLab menu just click on CI/CD -> Pipelines and then "Run pipeline". Click again on "Run pipeline" button to confirm.

![](docs/pipeline_1.png)

When the pipeline is running, click on the "Pipeline ID" for more details.

![](docs/pipeline_2.png)

To see the output, click on a job name.
![](docs/pipeline_output.png)

When the pipeline ends successfully, this will be the state:
![](docs/pipeline_end.png)


CI/CD file is stored [here](https://gitlab.com/stefanogalliani/test_project/-/blob/main/.gitlab-ci.yml) and it's organised in stages:

```
- development
- iac_deploy
- build
- deploy
```

### 3.1 - development stage
"development" stage takes care of development environment and testing it.
### 3.1.1 - development-stack job
"development-stack" job:
- replaces variables inside docker-compose files as described in "0.3 - docker-compose variables" section"
- installs docker-compose package
- runs docker-compose
- test the application stack
- shuts the docker-compose down

### 3.2 - iac_deploy stage
"iac_deploy" stage interacts with Terraform.
### 3.2.1 - terraform job
"terraform" job:
- replaces variables inside ecr_aws.tf in the same way as "development-stack" job does
- runs terraform init, plan and apply

### 3.3 - build stage
"build" stage sets up production environment and test it up.
### 3.3.1 - build-prod job
"build-prod" job:
- replaces variables inside docker-compose files as described in "0.3 - docker-compose variables" section"
- installs docker
- installs docker-compose
- runs the AWS ECR login
- tags the images
- pushes the images into AWS ECR

### 3.4 - deploy stage
"deploy" stage makes the production stack available.
### 3.4.1 - deploy-prod
"deploy-prod" job:
- replaces variables inside docker-compose files as described in "0.3 - docker-compose variables" section"
- installs docker
- install docker-compose
- runs the AWS ECR login
- bring the docker-compose up by pulling down images from ASW ECR

```
version: "3.9"
services:
  {SERVICE_APP_PROD}:
    image: ${AWS_REGISTRY}/${APP_PROD_TAG}
    deploy:
      restart_policy:
        condition: on-failure
  {SERVICE_NGINX_PROD}:
    image: ${AWS_REGISTRY}/${NGINX_PROD_TAG}
    deploy:
      restart_policy:
        condition: on-failure
    ports:
      - "8600:80"
    depends_on:
      - ${APP_PROD}

```
